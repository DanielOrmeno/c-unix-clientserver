#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

int strTok(char * s, char delim, char* toks[], int max_toks){
	int phase = 0;
	int c = 0;
	int bc = 0;
	while(1)
	{
		char ch = s[c];
		if(phase == 0)
		{
			toks[bc] = &s[c];
			phase = 1;
			bc++;
			if(bc == max_toks) break;
		}else if(ch == delim)
		{
			s[c] = '\0';
			phase = 0;
		}else if(ch == '\0')
		{
			break;
		}
		c++;
	}
	return bc;
}
		
int main (int argc, char * argv[]) {
	
	FILE *myFile;
	
	int numberOfLines, i, num;
	char *tokens[1000];
	char fileName [100];
	
	myFile = fopen(argv[1],"w"); //open file
	
	if(!myFile)	{
		printf("File could not be created");
		perror("Failed to create file");
		exit(1);
	}
	
	numberOfLines=strTok(argv[2], '\n', tokens, 99);
	
	for (i=0; i<numberOfLines;i++){
		fprintf(myFile, tokens[i]);
		fprintf(myFile,"\n");
	}
	fclose (myFile);
	
    return 0;
}



