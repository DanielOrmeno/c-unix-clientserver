#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main (int argc, char * argv[]){
	int numOfSeconds;
	if (argc==2){
	numOfSeconds=atoi(argv[1]);
	sleep(numOfSeconds);
	printf("Server response delayed by %d seconds\n",numOfSeconds);
	} else {
		printf("Error: Enter a valid number of seconds to delay");
	}
	
return 1;
}