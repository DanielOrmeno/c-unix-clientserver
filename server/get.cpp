#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

int strTok(char * s, char delim, char* toks[], int max_toks){
		int phase = 0;
		int c = 0;
		int bc = 0;
		while(1)
		{
			char ch = s[c];
			if(phase == 0)
			{
				toks[bc] = &s[c];
				phase = 1;
				bc++;
				if(bc == max_toks) break;
			}else if(ch == delim)
			{
				s[c] = '\0';
				phase = 0;
			}else if(ch == '\0')
			{
				break;
			}
			c++;
		}
		return bc;
}
		
int main (int argc, char * argv[]) {
	
	FILE *myFile;
	int numberOfLines, i, num;
	char *tokens[1000];
	char exten[5]= ".txt";
	char fileName [100], line [512];
	
	strcpy(fileName, argv[1]);
	strcat(fileName,exten);
	
	myFile = fopen(fileName,"r"); //open file
	
	if(!myFile)	{
		printf("File not found");
		perror("Failed to open file");
		exit(1);
	}
	
	num=fread(line, 1, 199, myFile);
	line[num]='\0';
	numberOfLines=strTok(line, '\n', tokens, 99);
	
	for (i=0; i<numberOfLines;i++){
		printf("%s\n",tokens[i]);
	}
	fclose (myFile);
	
    return 0;
}



