#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/utsname.h>

int main (int argc, char * argv[]){
 
	struct utsname thisSystem;
	char outputStr[512];
	if (uname( &thisSystem ) == -1) {

	  perror("uname function call has Failed.\n");
	  exit(1);
	}
	
	snprintf(outputStr, 511, "Server Name: %s\nServer Version:%s\nServer CPU:%s ",thisSystem.sysname, thisSystem.version,thisSystem.machine);
	printf("Server information:\n%s\n",outputStr);
return 1;
}