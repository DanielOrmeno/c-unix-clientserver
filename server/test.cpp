
bool handleUsrCmd(char usrCmd[256], int socketFd){
	int argsNum,len;
	char *tokens[100];
	bool ctrl = true;
	char * args[] = {NULL, NULL, NULL, NULL};							//Args for 4 possible commands	//array of char pointers for tokens
	
	len = strlen(usrCmd);
	if((usrCmd[len-1] == '\n')||(usrCmd[len-1] == ' ')) {				// Strips address for printf
		usrCmd[len-1] = 0;
	}
	
	argsNum = strTok(usrCmd, ' ', tokens, 99);							//Separate user command into tokens
	tokens[argsNum]=NULL;
	
	dup2(sock,1);
	
	if (argsNum>=5||argsNum<0){
		ctrl= false;													//To many arguments
	} else {
		if ((strcmp(tokens[0], "list"))==0){
			//printf("this is a list command\n");
			args[0]="list";
			execv ("list", args);
		} else if (((strcmp(tokens[0],"delay"))==0)&&((strcmp(tokens[1], "filepath"))==0)){
			printf("this is a get filepath command\n");
			
		} else if (((strcmp(tokens[0],"put"))==0)&&((strcmp(tokens[1], "localfile"))==0)){
			printf("this is a put localfile command\n");
			
		} else if (((strcmp(tokens[0], "sys"))==0)&&(argsNum==1)){
			printf("this is a sys command\n");
			
		} else if (((strcmp(tokens[0],"delay"))==0)&&((strcmp(tokens[1], "integer"))==0)){
			printf("this is a delay integer command\n");
			
		} else {
			ctrl= false;
		}
	}
	
	return ctrl;
}
