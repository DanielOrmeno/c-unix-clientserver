#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

int main (int argc, char * argv[]){

	DIR * d;
	char * dir_name ;
	char local[] =".";
	
	if (argc==1){								//If simple command
		
		dir_name=local;
		
	} else {									//Else Optional commands
			if ((strcmp(argv[1],"-l"))==0){
				
			} else if ((strcmp(argv[1],"-f"))==0){
				
			} else {
				dir_name=argv[1];
			}
	}
	
	//- OPEN CURRENT DIRECTORY
	d = opendir (dir_name);
	
	if (!d) {
		printf ("Cannot open directory: No such file or directory");
		perror ("Bad directory");
		exit (1);
	}
	
	while (1) {
		struct dirent * entry;
		
		entry = readdir (d);
		if (! entry) {
			break;
		}
		printf ("%s\n", entry->d_name);
	}
	
	//- CLOSE DIRECTORY
	if (closedir (d)) {
		perror ("Could not close directory");
		exit (1);
	}	
		

    return 0;
}











