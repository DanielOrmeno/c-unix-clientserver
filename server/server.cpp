// s2850944_7420_server.cpp : Defines the entry point for the console application.
//



//-WIN32 CODE

#if WIN32
#include "stdafx.h"
#include<io.h>
#include<stdio.h>
#include<winsock2.h>
#include <process.h>
#include <string.h>
#include <Windows.h>


#pragma comment(lib,"ws2_32.lib") //Winsock Library


int strTok(char * s, char delim, char* toks[], int max_toks){
		int phase = 0;
		int c = 0;
		int bc = 0;
		while(1)
		{
			char ch = s[c];
			if(phase == 0)
			{
				toks[bc] = &s[c];
				phase = 1;
				bc++;
				if(bc == max_toks) break;
			}else if(ch == delim)
			{
				s[c] = '\0';
				phase = 0;
			}else if(ch == '\0')
			{
				break;
			}
			c++;
		}
		return bc;
	}

bool handleUsrCmd(char * args[], SOCKET socketFd, int argsNum){
	bool ctrl = true;
	char message[250];
	//dup2(socketFd,1);
	
	if (argsNum>=5||argsNum<0){
		ctrl= false;													//To many arguments
	} else {
		if ((strcmp(args[0], "list"))==0){
			//execv ("list", args);
			printf("This is a list command, thats as far as I go unfortunatelly\n");
			strcpy(message, "Please try me again in Unix, i work fine there\n");
			send(socketFd, message, 200,0);
		} else if (((strcmp(args[0],"get"))==0)){
			//execv("get", args);
			printf("This is a get command, thats as far as I go unfortunatelly\n");
			strcpy(message, "Please try me again in Unix, i work fine there\n");
			send(socketFd, message, 200,0);
		} else if (((strcmp(args[0],"put"))==0)){
			//execv("put", args);
			printf("This is a put command, thats as far as I go unfortunatelly\n");
			strcpy(message, "Please try me again in Unix, i work fine there\n");
			send(socketFd, message, 200,0);
		} else if (((strcmp(args[0], "sys"))==0)&&(argsNum==1)){
			//execv("sys",args);
			printf("This is a sys command, thats as far as I go unfortunatelly\n");
			strcpy(message, "Please try me again in Unix, i work fine there\n");
			send(socketFd, message, 200,0);
		} else if (((strcmp(args[0],"delay"))==0)){
			int numOfSeconds;
			numOfSeconds=atoi(args[1]);
			if (numOfSeconds>0){
				numOfSeconds=numOfSeconds*1000;
				Sleep(numOfSeconds);
				strcpy(message, "Server delayed by ");
				strcat(message, args[1]);
				strcat(message, " seconds\n");
			} else {strcpy(message, "invalid argument for delay\n");}
			send(socketFd,message, 200,0);
			printf("I waited %s seconds",args[1]);
		} else {
			ctrl= false;
		}
	}
	
	return ctrl;
}

int handleRequest (SOCKET sock){
	
	int n, len, argsNum, i, recv_size;

	char buffer[500];													//Buffer for request
	char reply[500];
	char *tokens[4];
	
	memset(reply,0, 500);
	memset(buffer,0, 500);													//allocate zeros on buffer
	
	if((recv_size = recv(sock , buffer , 500 , 0)) == SOCKET_ERROR){
			puts("recv failed \n");
	}
	//Add a NULL terminating character to make it a proper string before printing
	buffer[recv_size] = '\0';
	
	len = strlen(buffer);
	
	if((buffer[len-1] == '\n')||(buffer[len-1] == ' ')) {				// Strips \n out of command
		buffer[len-1] = 0;
	}

	printf("this is the buffer %s:\n",buffer);
	
	
	argsNum = strTok(buffer, ' ', tokens, 500);							//Separate user command into tokens
	tokens[argsNum]=NULL;
	
	printf("\nclient->");

	if (argsNum>0&&argsNum<=3){
		printf("%s %s\n",tokens[0], tokens[1]);
	} else {
		printf("invalid command");
	}

	if (!handleUsrCmd(tokens, sock, argsNum)){										//Process user command
		 
		strncpy(reply, "Sorry, command not supported or is invalid\n",80);
		if ((n = write (sock, reply, 499))<0){
			perror ("error writing to socket");
			exit(1);
		}
	}
}

int main (int argc, char * argv[]) {
	
	int clientLength, recv_size;
	struct sockaddr_in client_socket;
	SOCKET mySocket, new_socket;
	char server_reply[250];
	
	printf("IMPORTANT: This server can only handle one request at a time, please run on UNIX for multiple requests/connections\n\n\n");
	//WINSOCK
	WSADATA wsa;
	if (WSAStartup (MAKEWORD(2,2),&wsa)!=0){
		printf("Error initialising winSock");
	}
	
	// CREATES A SOCKET
	mySocket = socket (AF_INET, SOCK_STREAM, 0);					// Family: IF_INET - PROTOCOL: TCP
	
	if (mySocket==INVALID_SOCKET){
		printf("Socket for connection could not be created");
	}

	// SERVER SETUP								
	struct sockaddr_in server;		
	server.sin_addr.s_addr=INADDR_ANY;							// Server IP Address
	server.sin_family=AF_INET;											// TCP protocol
	server.sin_port=htons(80);											// Server port 80//DEFAULT

	// CONNECTION TO SERVER (TCP)
	if(bind(mySocket , (sockaddr *)&server , sizeof(server))==SOCKET_ERROR){
		puts("Error connecting to server on bind: SOCKET_ERROR \n");
	} else { printf("Bind succesfull\n");}
	
	
	listen(mySocket, 10);												//Listens with an queue length of 10 connections
	clientLength = sizeof(client_socket);
	
	//- INFINITE LOOP TO ACCEPT MULTIPLE REQUESTS
	while (1){
	//- ACCEPTS INCOMMING CONNECTIONS
		new_socket = accept(mySocket, (struct sockaddr *)&client_socket, 
                                &clientLength);	
	
		if (new_socket==INVALID_SOCKET){
			printf("accept failed with error code: &d ", WSAGetLastError());
		} else {
			handleRequest(new_socket); //WINDOWS I HATE YOU
			//puts(server_reply);
			//send(new_socket, "done your shit", 20,0);
		}
	}
	closesocket(mySocket);
	WSACleanup();
	
	return 0;
}

//-UNIX CODE

#else

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <unistd.h>
#define SOCK_PATH "/dev/socket/echo_socket"


int strTok(char * s, char delim, char* toks[], int max_toks){
		int phase = 0;
		int c = 0;
		int bc = 0;
		while(1)
		{
			char ch = s[c];
			if(phase == 0)
			{
				toks[bc] = &s[c];
				phase = 1;
				bc++;
				if(bc == max_toks) break;
			}else if(ch == delim)
			{
				s[c] = '\0';
				phase = 0;
			}else if(ch == '\0')
			{
				break;
			}
			c++;
		}
		return bc;
	}

bool handleUsrCmd(char * args[], int socketFd, int argsNum){
	//int argsNum, len,i;
	bool ctrl = true;						//Args for 4 possible commands	//array of char pointers for tokens

	dup2(socketFd,1);
	
	if (argsNum>=5||argsNum<0){
		ctrl= false;													//To many arguments
	} else {
		if ((strcmp(args[0], "list"))==0){
			execv ("list", args);
		} else if (((strcmp(args[0],"get"))==0)){
			execv("get", args);
		} else if (((strcmp(args[0],"put"))==0)){
			execv("put", args);
		} else if (((strcmp(args[0], "sys"))==0)&&(argsNum==1)){
			execv("sys",args);
		} else if (((strcmp(args[0],"delay"))==0)){
			execv("delay",args);
		} else {
			ctrl= false;
		}
	}
	
	return ctrl;
}
	
int handleRequest (int sock){
	
	int n, len, argsNum, i;
	pid_t pid;
	char buffer[500];													//Buffer for request
	char reply[500];
	char *tokens[4];
	
	bzero(reply, 500);
	bzero(buffer, 500);													//allocate zeros on buffer
	
	if ((n= read(sock, buffer, 499))<0){
		perror ("error reading from socket");
		exit(1);
	}
	
	len = strlen(buffer);
	
	if((buffer[len-1] == '\n')||(buffer[len-1] == ' ')) {				// Strips \n out of command
		buffer[len-1] = 0;
	}
	
	argsNum = strTok(buffer, ' ', tokens, 500);							//Separate user command into tokens
	tokens[argsNum]=NULL;
	
	signal(SIGCHLD, SIG_IGN);											// Parent process wont wait
	if ((pid = fork())<0){												//Fork child process
		perror ("Could not fork child");
		exit(1);
	} else if (pid ==0){												//Child Process
		
		printf("\nclient->");
		
		if (argsNum>0&&argsNum<=3){
			printf("%s %s\n",tokens[0], tokens[1]);
		} else {
			printf("invalid command");
		}
		
		if (!handleUsrCmd(tokens, sock, argsNum)){										//Process user command
		 
			strncpy(reply, "Sorry, command not supported or is invalid\n",80);
		
			if ((n = write (sock, reply, 499))<0){
				perror ("error writing to socket");
				exit(1);
			}
		}
		
	} else {															//Parent Process
		return 1;
		
	}
	
}

int main (int argc, char * argv[]) {
	
	
	int listenFd, incomingFd, readn, writen, clientLength, count=0;
	pid_t pid;
	struct sockaddr_in server_socket, client_socket;					
	char requestBuffer [513], replyBuffer[513];							//Buffer to store client's requests
	
	if ((listenFd = socket( AF_INET, SOCK_STREAM, 0))<0){				//Initializes socket and checks for errors
		perror("socket creation error ");
		return(1);
	}
	
	//- SOCKET CONFIGURATION
	bzero ((char *) &server_socket, sizeof(server_socket));				//Sets zeros to address structure
	server_socket.sin_family = AF_INET;									//TCP family in sockaddr_in structure
	server_socket.sin_addr.s_addr=INADDR_ANY; 							//Sets to any available address
	server_socket.sin_port=htons(80);									//Default port number 80;
	
	char ip4 [INET_ADDRSTRLEN];											//Server's ip address
	inet_ntop(AF_INET, &(server_socket.sin_addr), ip4, INET_ADDRSTRLEN);
	printf("Server listening on address: %s\n", ip4);
	
	//- SOCKET BINDED AND LISTENING FOR TCP CONNECTIONS 
	
	if ((bind(listenFd, (struct sockaddr *) &server_socket,
                          sizeof(server_socket)))< 0){					//Binds and checks for errors.						
		perror("Binding error");
		return(1);
	} else { printf("\nBind successful\n"); }
	
	
	listen(listenFd, 10);												//Listens with an queue length of 10 connections
	clientLength = sizeof(client_socket);
	
	//- INFINITE LOOP TO ACCEPT MULTIPLE REQUESTS
	while (1){
		
		signal(SIGCHLD, SIG_IGN);										// Parent process wont wait
		
		//- ACCEPTS INCOMMING CONNECTIONS
		incomingFd = accept(listenFd, (struct sockaddr *)&client_socket, 
                                &clientLength);	
		
		if (incomingFd < 0){											// Checks for errors when accepting
			perror("ERROR on accept");	
			return(1);
			
		} else {														//Accept successful, forks
			pid = fork ();
			if (pid<0){													
			perror ("Could not fork child");
			return(1);
			}
			
			if (pid==0){												//Child process
				close(listenFd);										//Close listening port
				handleRequest(incomingFd, count);						//Process request
				close (incomingFd);
				exit(0);
			} else {													//Parent process
				count++;
				close (incomingFd);
			}
		}
	}
	
	return 0;
}

#endif













