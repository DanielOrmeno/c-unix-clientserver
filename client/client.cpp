/* 
s2850944_7420_client.cpp
*/


//-Windows Code
#if WIN32

#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <WinSock2.h>


#pragma comment(lib,"ws2_32.lib") //Winsock Library

int strTok(char * s, char delim, char* toks[], int max_toks){
		int phase = 0;
		int c = 0;
		int bc = 0;
		while(1)
		{
			char ch = s[c];
			if(phase == 0)
			{
				toks[bc] = &s[c];
				phase = 1;
				bc++;
				if(bc == max_toks) break;
			}else if(ch == delim)
			{
				s[c] = '\0';
				phase = 0;
			}else if(ch == '\0')
			{
				break;
			}
			c++;
		}
		return bc;
	}

void sendquery(char address[16], char message[250]){

	// INITIALISATION OF WINSOCK
	WSADATA wsa;

	if (WSAStartup (MAKEWORD(2,2),&wsa)!=0){
		printf("Error initialising winSock");
	}

	// CREATES A SOCKET
	SOCKET mySocket = socket (AF_INET, SOCK_STREAM, 0);					// Family: IF_INET - PROTOCOL: TCP
	
	if (mySocket==INVALID_SOCKET){
		printf("Socket for connection could not be created");
	}

	// SERVER SETUP								
	struct sockaddr_in server;		
	server.sin_addr.s_addr=inet_addr(address);							// Server IP Address
	server.sin_family=AF_INET;											// TCP protocol
	server.sin_port=htons(80);											// Server port 80//DEFAULT

	// CONNECTION TO SERVER (TCP)
	if(connect(mySocket , (sockaddr *)&server , sizeof(server))==SOCKET_ERROR){
		puts("Error connecting to server on bind: SOCKET_ERROR \n");
	}


	// SEND REQUESTS TO SERVER
	char * clientRequest = message; //"GET / HTTP/1.1\r\n\r\n";
	send (mySocket, clientRequest, strlen(clientRequest), 0);
	
	//Receive a reply from the server
	char server_reply[2000];
    int recv_size;
    if((recv_size = recv(mySocket , server_reply , 2000 , 0)) == SOCKET_ERROR){
        puts("recv failed \n");
	}

    //Add a NULL terminating character to make it a proper string before printing
    server_reply[recv_size] = '\0';
    puts(server_reply);

	closesocket(mySocket);
	WSACleanup();
}

bool validAddress(char address[16]){

	if(inet_addr(address)!=INADDR_NONE){
			printf("The address is Valid\n\n");
			return true;
		} else {
			printf("Invalid Address, please try again\n\n"); 
			return false;
	}
}

void sendFile(char address[16], char fileName[100]){
	
	FILE *myFile;
	int numberOfLines, i, num, len, len1;
	char *tokens[100]; 
	char line [1000];
	char message[1000];
	char exten[5]= ".txt";
	
	memset(message, 0, 999);
	
	len = strlen(address);											//Length of address for stripping \n char
	len1 = strlen(fileName);										//Length of fileName for stripping \n char
	 
	if( fileName[len1-1] == '\n' ) {								// Strips address for printf
		fileName[len1-1] = 0;
	}
	
	strcat(fileName, exten);
																	//Add a NULL terminating character 
	if( address[len-1] == '\n' ) {									// Strips address for printf
		address[len-1] = 0;
	}
	myFile = fopen(fileName,"r"); //open file
	
	if(!myFile)	{
		perror("Failed to open file");
		return;
	}
	
	num=fread(line, 1, 500, myFile);
	line[num]='\0';
	
	strcat(message, "put ");
	strcat(message, fileName);
	strcat(message, " ");
	strcat(message, line);
	
	sendquery(address,message);
	
	fclose (myFile);
}

int main(int argc, char * argv[]){
	//- GET SERVER ADDRESS FROM USER
	printf("%s\n",argv[1]);
	

	char serverAddress[16];
	bool adrCtrl=true;
	
	if (validAddress(argv[1])){
		strncpy(serverAddress, argv[1],15);
		serverAddress[15]='\0';
	} else {
		puts(argv[1]);
		printf("Invalid address");
		exit(1);
	}

	printf ("Welcome, \nThis program will allow you to query a remote server (%s) listening on port 80.\n\n", serverAddress);
	
	printf("Insert queries now, to exit type quit\n\n");
	
	//- LOOP FOR QUERIES TO SELECTED SERVER
	char userMessage[1000];
	char temp[100];
	char checkQuit[5];
	bool queryCtrl = true;
	int querySize;
	char * tokens[100];

	while(queryCtrl){
		printf ("->");
		fgets (userMessage, sizeof(userMessage), stdin);				// Gets user input
		strncpy(checkQuit, userMessage, 4);								// Parse quit value to smaller string for compare
		checkQuit[4]='\0';												//null terminate last character

		if (strcmp("quit",checkQuit) == 0){
			queryCtrl=false;											//Quits loop and ends program.
			printf("Bye bye\n\n");
		} else{
			strncpy(temp,userMessage,99);
			querySize = strTok(temp, ' ', tokens, 99);			//Separate user command into tokens
			tokens[querySize]=NULL;
		
			if ((strcmp("put", tokens[0])==0)&&(querySize>1)){			//If message starts with put
				//sendFile(serverAddress, tokens[1]);						//name of file in args
			} else {
				sendquery (serverAddress, userMessage);
			}
		}
	}
	return 0;

}

//-UNIX CODE
#else

//UNIX CODE
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int strTok(char * s, char delim, char* toks[], int max_toks){
		int phase = 0;
		int c = 0;
		int bc = 0;
		while(1)
		{
			char ch = s[c];
			if(phase == 0)
			{
				toks[bc] = &s[c];
				phase = 1;
				bc++;
				if(bc == max_toks) break;
			}else if(ch == delim)
			{
				s[c] = '\0';
				phase = 0;
			}else if(ch == '\0')
			{
				break;
			}
			c++;
		}
		return bc;
	}

bool validAddress(char address[16]){

	if(inet_addr(address)!=INADDR_NONE){
			//printf("The address is Valid\n\n");
			return true;
		} else {
			printf("Invalid Address, please try again\n\n"); 
			return false;
	}
}

void sendquery(char address[16], char message[1000]){

	int requestFd, readn, len;								
	struct sockaddr_in server_address;								
	char serverAddress[15];
	
	len = strlen(address);
	
	if ((requestFd = socket( AF_INET, SOCK_STREAM, 0))<0){			//Initializes socket and checks for errors
		perror("socket creation error");
		exit(1);
	}
	
	//- SOCKET CONFIGURATION
	bzero ((char*) &server_address, sizeof(server_address));		//Sets zeros to address structure
	server_address.sin_addr.s_addr=inet_addr(address);				//Sets Ip Address (input by client)
	server_address.sin_family = AF_INET;							//TCP family in sockaddr_in structure
	server_address.sin_port=htons(80);								//Default port number 80;
	
	//- CONNECTS TO SERVER
	if (connect (requestFd, 
	(sockaddr*)&server_address, sizeof(server_address))<0){			//Binds and checks for errors.						
		perror("Connection to server failed");
		exit(1);
	}
	
	// SEND REQUESTS TO SERVER
	readn = write (requestFd, message, strlen(message));
	if (readn<0){
		perror("Error writing to socket");
		exit(1);
	}
	
	
	//Receive a reply from the server
	char server_reply[2000];
    int recv_size;
    if((recv_size = read(requestFd , server_reply , 1999))<0){
        perror("read failed \n");
		exit(1);
    }
	
	if( address[len-1] == '\n' ) {									// Strips address for printf
		address[len-1] = 0;
	}
	
	printf("%s-> ", address);								//Add a NULL terminating character 
																	//to make it a proper string before printing
    server_reply[recv_size] = '\0';
    puts(server_reply);
	
	//- TERMINATE CONNECTION AND CLOSE SOCKET
	close(requestFd);
}

void sendFile(char address[16], char fileName[100]){
	
	FILE *myFile;
	int numberOfLines, i, num, len, len1;
	char *tokens[100]; 
	char line [1000];
	char message[1000];
	char exten[5]= ".txt";
	
	bzero(message,999);
	
	len = strlen(address);											//Length of address for stripping \n char
	len1 = strlen(fileName);										//Length of fileName for stripping \n char
	 
	if( fileName[len1-1] == '\n' ) {								// Strips address for printf
		fileName[len1-1] = 0;
	}
	
	strcat(fileName, exten);
																	//Add a NULL terminating character 
	if( address[len-1] == '\n' ) {									// Strips address for printf
		address[len-1] = 0;
	}
	myFile = fopen(fileName,"r"); //open file
	
	if(!myFile)	{
		perror("Failed to open file");
		return;
	}
	
	num=fread(line, 1, 500, myFile);
	line[num]='\0';
	
	strcat(message, "put ");
	strcat(message, fileName);
	strcat(message, " ");
	strcat(message, line);
	
	sendquery(address,message);
	
	fclose (myFile);
}

int main (int argc, char * argv[]) {
	
	//- GET SERVER ADDRESS FROM USER
	char serverAddress[16];
	bool adrCtrl=true;
	
	if (validAddress(argv[1])){
		strncpy(serverAddress, argv[1],15);
		serverAddress[15]='\0';
	} else {
		puts(argv[1]);
		printf("Invalid address");
		exit(1);
	}
	
	printf ("Welcome, \nThis program will allow you to query a remote server (%s) listening on port 80.\n\n", serverAddress);
	
	printf("Insert queries now, to exit type quit\n\n");
	
	//- LOOP FOR QUERIES TO SELECTED SERVER
	char userMessage[1000];
	char temp[100];
	char checkQuit[5];
	bool queryCtrl = true;
	int querySize;
	char * tokens[100];

	while(queryCtrl){
		printf ("->");
		fgets (userMessage, sizeof(userMessage), stdin);				// Gets user input
		strncpy(checkQuit, userMessage, 4);								// Parse quit value to smaller string for compare
		checkQuit[4]='\0';												//null terminate last character

		if (strcmp("quit",checkQuit) == 0){
			queryCtrl=false;											//Quits loop and ends program.
			printf("Bye bye\n\n");
		} else{
			strncpy(temp,userMessage,99);
			querySize = strTok(temp, ' ', tokens, 99);			//Separate user command into tokens
			tokens[querySize]=NULL;
		
			if ((strcmp("put", tokens[0])==0)&&(querySize>1)){			//If message starts with put
				sendFile(serverAddress, tokens[1]);						//name of file in args
			} else {
				sendquery (serverAddress, userMessage);
			}
		}
	}
	return 0;
	
}










#endif






